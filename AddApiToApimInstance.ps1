# Api Management service specific details

$subscriptionId = "df85cf12-711e-437d-888b-9db265dcd8b2"
$apimServiceName = "ftidevapi"
$resourceGroupName = "RG-apim-DevAppGw"
$location = "westus"
$organisation = "franklintempleton.com"
$adminEmail = "yallojirao.chavan@franklintempleton.com"

# Api Specific Details                                
$swaggerUrl = "https://bitbucket.org/ychavan12/http-ychavan-stash.noam.corp.frk.com-7990-scm-webapi/raw/e12bfeb97214bfa415a13ac53f423f605970f717/fundsCitiProductList.json"
$apiPath = "CitiNext"

# File paths for Policy and Configuration
$ApiNextCityPolicy = "https://bitbucket.org/ychavan12/http-ychavan-stash.noam.corp.frk.com-7990-scm-webapi/raw/4e63461f51b00c59cd048b0813002d116b2c919e/ApiLevelPolicy"
$OpProdListPolicy = "https://bitbucket.org/ychavan12/http-ychavan-stash.noam.corp.frk.com-7990-scm-webapi/raw/4e63461f51b00c59cd048b0813002d116b2c919e/productListPolicy"
$OpProdDetailsPolicy = "C:\Devops\_FundCiti\OperationLevelPolicy\GET__product-details.xml"


# Get the API Management context
$Apicontext = New-AzureRmApiManagementContext -ResourceGroupName $resourceGroupName -ServiceName $apimServiceName
#Get-AzureRmApiManagementApi -Context $context -Name "2bcdbbec60044889af8e7c6e41b7303c" -SpecificationUrl
$api = Import-AzureRmApiManagementApi -Context $Apicontext -SpecificationFormat Swagger  -SpecificationPath  $swaggerUrl -Path $apiPath

# $api = Get-AzureRmApiManagementApi -Context $context -Name "2bcdbbec60044889af8e7c6e41b7303c"
# Set-AzureRmApiManagementPolicy -Context $apimContext -ApiId "9876543210" -OperationId "777" -PolicyPath $PolicyString
# Get a Product  Imported Api. 
$fundcitiProdID = Get-AzureRmApiManagementProduct -Context $Apicontext -ProductId "fundciti-prod"
#$Productid = Get-AzureRmApiManagementProduct -Context $context  -ProductId "725a2dde48fc47eb85ed2935c8db85fe" 
# Add the petstore api to the published Product, so that it can be called in developer portal console
Add-AzureRmApiManagementApiToProduct -Context $Apicontext -ProductId $fundcitiProdID.productid  -ApiId $api.ApiId 

# update Policy file

$GetApiId = Get-AzureRmApiManagementApi -Context $Apicontext   -Name "Next_Citi_Testy" 

$GetOperationProdList = Get-AzureRmApiManagementOperation -Context $Apicontext -ApiId $GetApiId.ApiId -OperationId "productlist"
$GetOperationProdDetail = Get-AzureRmApiManagementOperation -Context $Apicontext -ApiId $GetApiId.ApiId -OperationId "ProductDetails"

Set-AzureRmApiManagementPolicy -Context $Apicontext -ApiId $GetApiId.ApiId  -PolicyFilePath $ApiNextCityPolicy

Set-AzureRmApiManagementPolicy -Context $Apicontext -ApiId $GetApiId.ApiId  -OperationId $GetOperationProdList.OperationId -PolicyFilePath $OpProdListPolicy 
#Set-AzureRmApiManagementPolicy -Context $Apicontext -ApiId $GetApiId.ApiId  -OperationId $GetOperationProdDetail.OperationId -PolicyFilePath $OpProdDetailsPolicy 